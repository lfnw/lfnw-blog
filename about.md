---
layout: page
title: About
permalink: /about/
redirect_to: https://discuss.lfnw.org/t/about-the-lfnw-blog-category/135/1
---

_Welcome to the new home of the LFNW blog, the primary source of news about LFNW._

We’re moving the blog here in order to make it easier for more LFNW contributors to post content, and to foster increased communication with the community.
