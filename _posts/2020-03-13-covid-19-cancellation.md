---
layout: post
title:  "LFNW2020 COVID-19 Cancellation Statement"
date:   2020-03-13 12:00:00 -0700
tags: [LFNW2020, Coronavirus, COVID-19, LFNW2021]
fb_post_id: 10157003955522286
tweet_id: 1238548688059822081
redirect_to: https://discuss.lfnw.org/t/lfnw2020-covid-19-cancellation-statement/137/1
---

With an abundance of precaution and care for the well-being of all LinuxFest presenters, sponsors, attendees, volunteers, and their loved ones, we have unanimously elected to not hold LFNW in-person at Bellingham Technical College this year. We were all looking forward to another excellent year of the Fest, but the role that events like ours can have in spreading the virus, already present in our locality, is too great to ignore.  In the coming weeks, we will be connecting with the accepted presenters to find ways of bringing their talks to our website and YouTube channel, and setting up a space for Q&A. We will also be reaching out to our Individual Supporters who may elect to maintain their tax deductible donations in support of the Fest, or be fully refunded. Our Corporate Sponsors may choose to receive a full refund, or roll over their sponsorship to next year’s LinuxFest Northwest in April of 2021.

Stay tuned to https://blog.lfnw.org and our [Twitter feed](https://twitter.com/lfnw/) for updates, and please do not hesitate to [contact us](mailto:info@linuxfestnorthwest.org) with any remaining questions. _Stay safe, wash your hands, we’ll keep y’all up to date._
