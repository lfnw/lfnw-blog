---
layout: post
title:  "Session scheduling plan"
date:   2019-02-09 00:00:00 -0800
tags: [LFNW2019, call-for-papers, planning]
redirect_to: https://discuss.lfnw.org/t/session-scheduling-plan/151/1
---

First off, we'd like to thank everyone who submitted a proposal to LFNW2019... we have some _amazing_ content this year, and choosing which sessions will be presented is a hard choice. We've never had so many excellent proposals to choose from; whittling it down to our usual 80 sessions is a challenge!

We've heard from a few of you, that you're anxious to know about your session, and we hear you. Here's the current schedule, so you know what to expect:

* Call for proposals closed on Jan 31.
* **We're reviewing proposals, and will send out most acceptance and rejection notices by February 14th (+2 weeks - Happy Valentine's day).**
* We need to hear back from you, confirming your accepted sessions, by February 28th (+2 weeks).
* We will publish the session list on March 1st.
* We will submit a draft schedule to presenters shortly thereafter, and expect to have a final schedule with all room assignments and times posted by March 15th.

We hope this helps you plan, and we look forward to talking to our presenters more often moving forward. Feel free to [email us](mailto:info@linuxfestnorthwest.org) if you have any questions!
