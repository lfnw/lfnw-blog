---
layout: post
title: "T-shirt preorder closes on 3/31"
date: 2019-03-25 15:00:00 -0700
tags: [LFNW2019, tickets, t-shirts]
redirect_to: https://discuss.lfnw.org/t/t-shirt-preorder-closes-on-3-31/149/1
---

<div class="row justify-content-around">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <a href="{{ "/assets/img/2019-t-shirt-design.png" | prepend: site.baseurl }}">
            <img src="{{ "/assets/img/2019-t-shirt-design.png" | prepend: site.baseurl }}" alt="LFNW2019 t-shirt design" title="LinuxFest Northwest 2019: Past, Present & Future" class="img-fluid">
        </a>
    </div>
</div>

We usually keep our t-shirt design to ourselves until the Fest, but this year we just couldn't. We'll leave it up to you to decide if that's because it's so awesome this year, or if we just weren't ready in prior years ;-) _Please note, we haven't finalized the shirt & printing colors; the colors pictured are just for design work._

While we will have some for sale on site during [LFNW2019](https://https://lfnw.org/conferences/2019), the _only way to guarantee that you get a shirt_ is to [register](https://lfnw.org/conferences/2019/register/new) for the event, and buy an [Individual Sponsorship](https://lfnw.org/conferences/2019/tickets). The sponsorship provides us **$80**, and provides you:

* a limited edition LFNW2019 t-shirt (sizes S - 3XL)
* vouchers for lunch on both Saturday & Sunday
* stickers, patches, and any other cool stuff we make
* a pre-printed event badge (details forthcoming - but even our badge is cool this year)
* our [eternal gratitude](https://www.youtube.com/watch?v=4bqSkBCQwEw), for helping keep the Fest funded

**BUT HURRY UP!** _Individual Sponsorship will only be available until the end of the month, 3/31_. We'll do a little math, then order all your t-shirts on April 1st, so we can guarantee we have them at the Fest. This is a little different from prior years, where we've continued to accept orders, but with the caveat that your t-shirt may be late (and if you're that one guy from last year - your shirt is packed and just needs to go to the post office - SORRY). This year, we're not messing around with late shirts; order by the deadline, and we guarantee, or you can take your chances on-site!

**One more thing** about those event badges... we're pre-ordering some really cool badges, but that means we need to know who to print for beforehand, so if you are [registered](https://lfnw.org/conferences/2019/register/new) by 3/31 (no ticket purchase required), we will pre-print a badge and have it ready for you at the Fest. If you register after, we will have blank badges available for you to write your name on, but it just won't be as spiffy as the pre-printed ones. (I'll explain why in an upcoming post!)
