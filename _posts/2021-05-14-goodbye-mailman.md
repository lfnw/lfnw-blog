---
layout: post
title:  "Goodbye Mailman"
date:   "2021-05-14 00:00:00 -0800"
tags:   [infra]
redirect_to: https://discuss.lfnw.org/t/goodbye-mailman/198/5
---

Over the next week or so, LFNW will be migrating our mailing lists over to dedicated categories in our [Discourse server](https://discuss.lfnw.org).

### Why?

Well, a few reasons:

* Our [Discourse server](https://discuss.lfnw.org) offer all the benefits of the mailing list, plus a more feature rich web interface to boot.
* Discourse provides a much nicer way to thread people into conversations, and will allow more cross-interaction with the overall community - helping bring BLUG and LFNW closer together.
* Our discussion forums are professionally hosted and managed.
* Our mailing lists are currently managed on a virtual server running mailman, and we're just not keen on maintaining it anymore.

Thanks for understanding!
