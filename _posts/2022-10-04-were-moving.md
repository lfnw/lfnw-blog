---
layout: post
title:  "We're moving!"
date:   2022-10-04 12:00:00 -0800
tags: lfnw,blog
# fb_post_id: ?
# tweet_id: ?
# instagram_post_id: ?
# mastodon_toot_id: ?
redirect_to: https://discuss.lfnw.org/t/we-re-moving/445
---
This will be the last post on this blog. Moving forward, we will be posting content directly to the [LFNW Blog category](https://discuss.lfnw.org/c/lfnw-blog/10) of our [discussion forum](https://discuss.lfnw.org/).

**Why?** This blog, while beautiful, is a statically generated site, and only a limited number of LFNW organizers have the rights and the ability to post here. In order to make our blog more accessible, and increase the content posted there, we want all of our organizers to be able to post. Since we were already mirroring the blog posts over to [discuss.lfnw.org](https://discuss.lfnw.org/c/lfnw-blog/10) anyway, it just made sense to have that category _become the blog_.

You will no longer have to go somewhere else to comment, and we don't have to worry about keeping things in sync... everybody wins.

In about a month we'll go ahead and redirect this site straight over to the new blog.
