---
layout: post
title:  "2019 Call for Papers open!"
date:   2018-10-31 10:13:00 -0700
tags: [LFNW2019, call-for-papers]
fb_post_id: 10155857676202286
tweet_id: 1057682134851178496
redirect_to: https://discuss.lfnw.org/c/lfnw-blog/10
---
Big news! Call for papers is open at [LFNW.org](https://lfnw.org)! Our theme is "Past, Present, & Future" so let's talk about where we have been and where we are going with free and open source technology.
<!--more-->

_Please re-share!_
