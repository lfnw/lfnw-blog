---
layout: post
title:  "Nice and Nerdy"
date:   2018-11-18 05:23:00 -0800
tags: [youtube, lunduke, review]
fb_post_id: 10155895970887286
tweet_id: 1064328387848806400
redirect_to: https://discuss.lfnw.org/c/lfnw-blog/10
---
Tried and true friend of the fest, [Bryan Lunduke][1], had some very [<i class='fab fa-youtube'></i> nice and nerdy][2] things to say about us.

_Oh Bryan, you're too kind!_

[1]: http://lunduke.com/
[2]: https://www.youtube.com/watch?v=SB64JxKF1ro
