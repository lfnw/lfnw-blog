---
layout: post
title: "LFNW2019 Sponsorship is a Win Win!"
date: 2018-11-15 11:44:00 -0800
tags: [LFNW2019, call-for-sponsors]
fb_post_id: 10155888151002286
tweet_id: 1063155694973661187
redirect_to: https://discuss.lfnw.org/c/lfnw-blog/10
---
If your company loves and supports and contributes to free/open source software, a LFNW sponsorship would be a win-win! Check out our [prospectus](https://res.cloudinary.com/hcgshkmdf/image/upload/v1535423398/onqev97kcocyiatptsnj.pdf) and [contact us](mailto:sponsor@linuxfestnorthwest.org?subject=2019%20Sponsorship) for more juicy details at [lfnw.org](https://lfnw.org/conferences/2019#sponsors).
