---
layout: post
title:  "LFNW and Coronavirus"
date:   2020-03-09 20:00:00 -0700
tags: [LFNW2020, Coronavirus, COVID-19]
tweet_id: 1237409566129881094
fb_post_id: 10156995860122286
redirect_to: https://discuss.lfnw.org/t/lfnw-and-coronavirus/138/1
---

The LFNW organizing committee will be meeting this Thursday, Mar ~~11~~ 12, at 7PM PDST. Our #1 topic of business is to plan how LFNW2020 will be affected by the COVID-19 outbreak. As soon as we have a concrete plan of action, we'll post it here, on social media, and inform sponsors, presenters, and registered attendees via email.

_Thanks for you patience while we sort things out._
