---
layout: post
title:  "LFNW22 Call for Proposals now open!"
date:   2021-10-14 00:00:00 -0800
tags: [LFNW22, call-for-papers, 2022]
fb_post_id: 10158498340692286
tweet_id: 1449442903470731264
# instagram_post_id: ?
# mastodon_toot_id: ?
redirect_to: https://discuss.lfnw.org/t/lfnw22-call-for-proposals-now-open/349/5
---

We're officially announcing _LinuxFest Northwest 22_. We're trying one last time with our 2020 theme: **Be Excellent To Each Other!** We still want to share with you, and you with us, a conference that encourages collaboration, teaching, and lifting each other up, in a proper collaborative online experience. Special thanks to [Blindside Networks](https://blindsidenetworks.com/) who will be professionally hosting our [BigBlueButton](https://bigbluebutton.org/) virtual meeting space.

Our **call for proposals** is open _now through January 31, 2022_. We're sticking with our 2020 [session types](#session-types), [tracks](#tracks), and [difficulty levels](#difficulty-levels). _Read on for more details._

Our **call for sponsors** will open soon - reimagined to lift up communities friendly to free & open source software and philosophies. We'll be giving a special lift to our 2020 sponsors who have stuck with us and turned their financial sponsorships into donations with no obligations. More to come soon on this topic.

<!--more-->

#### _3rd time is a charm._

LinuxFest Northwest 22 is officially on, but not quite how you imagined it. With uncertainty still looming about the state of the COVID-19 pandemic, and a strong ethos of health & safety among our organizers, we're planning a virtual-first but hopefully hybrid event. What does that mean? Well, if you're up for presenting or attending, distance is no issue - all you need is a stable internet connection. If you miss the cameraderie of our social side, we'll be trying some interesting new experiments to accomodate you - and hopefully those can be in conjunction with some safe in-person activities in the Spring (but no promises at this point).

We're especially thankful to have [Blindside Networks](https://blindsidenetworks.com/) onboard this year. Thanks to their generous support, we'll be running live sessions with all the best tools, using the open source [BigBlueButton](https://bigbluebutton.org/) project. No prerecorded videos - sessions will be live, you'll be able to ask questions, follow slides, collaborate in breakouts, brainstorm on whiteboards, take notes, and chat with your fellow attendees... all on free open source software.

Any hybrid/in-person content will be added pretty late in the schedule, when we have a firm understanding of what we can manage responsibly, so assume virtual content, but be open to changing plans if you're up for in-person stuff. (We'd love to see you :)

### Call for Proposals

Our **call for proposals** is open _now through January 31, 2022_. We're sticking with our 2020 [session types](#session-types), [tracks](#tracks), and [difficulty levels](#difficulty-levels). Please sign up & submit your proposal at [https://lfnw.org/conferences/2022/program/proposals/new](https://lfnw.org/conferences/2022/program/proposals/new)!

#### <br/>Session Types

We've changed up the session types, to clarify what happens during those sessions:

*   **Lecture and Long Lecture**
    These are your 'typical' sessions - slide decks and other presentations. Session lengths stay the same, at 45 and 90 minutes + some time for questions and answers.
*   **Lab and Short Lab**
    These are hands-on sessions - we used to call these _Tutorials_, but that was confusing, as many tutorials are submitted, but they're not _hands on_. _Lab_ sessions are just under 2 hours; _Short Lab_ sessions are about an hour. This is a little shorter than in prior years, but it's going to make it easier for you to attend both labs and lectures.
*   **Assembly**
    Want to lead a Q&A, have a discussion about a topic, or set up a panel discussion? This is your session type.
*   **Recess**
    Are you interested in organizing a fun, collaborative activity _outside_ the lecture room? Here's your session type! Take attendees on a virtual tour, collaborate on an online art project, lead a guided meditation... you get the idea.

#### <br/>Tracks

You'll notice our normal list of tracks is missing this year... we have only one track for you to keep in mind as you make your proposals - our theme: **Be Excellent To Each Other**. We'll derive more granular tracks based on the accepted sessions... we just wanted to see what emerges this year, instead of you shoehorning your amazing session into a track list that didn't quite fit.

#### <br/>Difficulty Levels

Nobody wants to submit a _hard_ session... we get that, but we want to encourage sessions that dive deeply into a narrow topic, and we do not want to discourage sessions that require some previous experience. These can be exceptionally valuable sessions. So, we've changed our difficulty levels to be a bit less judgmental:

*   **100-level**
    Events are understandable for everyone without knowledge of the topic.
*   **200-level**
    Events require a basic understanding of the topic.
*   **300-level**
    Events require expert knowledge of the topic.

We hope this will clarify the level as based on _your experience as an attendee_, and not discourage proposals that are challenging, or require some prior knowledge.

**[Now go submit some excellent proposals!](https://lfnw.org/conferences/2022/program/proposals/new)**
