---
layout: post
title:  "Magic Badges!"
date:   2019-04-24 21:00:00 -0700
tags: [LFNW2019, badges, registration]
redirect_to: https://discuss.lfnw.org/t/magic-badges/148/1
---

Did you register for LinuxFest Northwest before April 15th? If so, you're in for a treat! Your badge will be pre-printed and ready to go at the registration desk. Just bring your ID so we can find it easily!

Why pre-print the badges? Well, we wanted to do some things special this year, in honor of our 20th anniversary, and that seeped all the way down to your badge. Coincidentally, when you're finished with it, your badge itself should be placed somewhere where it can be seeped... so the flowers can grow! Instead of a dust collector, or a piece of trash, your badge is made of plantable seed paper, and can sprout hundreds of Five Spot, Candytuft, Baby Blue Eyes, Siberian Wallflower,  Scarlet Flax, Zinnia, Sweet William Pinks,  Corn Poppy, Spurred Snapdragon, Catchfly, English Daisy, and Black-Eyed Susan.

<!--more-->

<div class="row justify-content-around">
    <div class="col-sm-12 col-md-8 col-lg-6">
        <img src="{{ "/assets/img/2019-badge.png" | prepend: site.baseurl }}" alt="LFNW2019 sample badge" title="LinuxFest Northwest 2019 seed paper badge" class="img-fluid">
    </div>
</div>
