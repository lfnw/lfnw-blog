---
layout: post
title:  "One more hotel choice for your LFNW visit"
date:   2019-03-19 14:00:00 -0700
tags: [LFNW2019, lodging]
redirect_to: https://discuss.lfnw.org/t/one-more-hotel-choice-for-your-lfnw-visit/150/1
---

There are a lot of people coming to LinuxFest Northwest this year! We've been working with our hotel partners to make sure we have nice, appropriate accomodations available for everyone who visits, and we now have one more discount option for LFNW2019 attendees.

_Comfort Inn Bellingham is offering a 15% discount for LFNW2019 attendees. Use the code "**LSN15**" when making a reservation._

> Welcome to the Comfort Inn Bellingham! Our hotel is located in Bellingham's Cordata shopping district, and about 3.3 miles from Bellingham Technical College.  Our hotel offers a free hot buffet breakfast including waffles, sausage and eggs, bacon, fruit, yogurt, and baked goods. Guests have access to the indoor pool, spa, sauna and exercise room. Our rooms have a mini-refrigerator, microwave, and we're 100% non-smoking.  Complimentary WIFI is available in all guest rooms and common areas. We offer complimentary parking and free airport transportation to Bellingham airport.  During your stay, you can visit nearby attractions such as Mount Baker Theatre, Squalicum Harbor and Bellingham Bay.  Don’t forget to ask for the LinuxFest Northwest discount code "**LSN15**" when making reservations by calling the hotel direct at _(360) 738-1100_. We look forward to hosting you for LinuxFest Northwest 2019.

This is in addition to the three options already available on the [site](https://lfnw.org/conferences/2019#lodging):

* [Heliotrope Hotel](https://www.heliotropehotel.com/)
  Reserve your room by March 26, 2019 using the Code: "**LFNW19**" to access the discounted group rate for arrival on April 26, 2019 to departure on April 28, 2019.
* [Holiday Inn & Suites](https://www.ihg.com/holidayinn/hotels/us/en/bellingham/blihi/hoteldetail?newRedirect=true&qIta=99801505&icdv=99801505&qSlH=BLIHI&qGrpCd=LFN&setPMCookies=true&qDest=4260%20Mitchell%20Way,%20Bellingham,%20WA,%20US&srb_u=1)
  Come experience all that the Holiday Inn and Suites-Bellingham has to offer! Mention group code "**LFN**" for our discounted group rate.
* [Home2 Suites by Hilton](https://secure3.hilton.com/en_US/ht/reservation/book.htm?inputModule=HOTEL&ctyhocn=BLINWHT&spec_plan=CHTLF5&arrival=20190426&departure=20190429&cid=OM,WW,HILTONLINK,EN,DirectLink&fromId=HILTONLINKDIRECT)
  The Home2 Suites is Bellingham’s only Hilton Hotel offering free Wi-Fi and breakfast. Book your room online or call us at _360-734-3111_ and ask for the "**LF5**" code.

Wherever you choose to stay, we're looking forward to seeing you the last weekend of April!
