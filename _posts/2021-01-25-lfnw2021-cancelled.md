---
layout: post
title:  "LFNW2021 Cancelled"
date:   2021-01-25 00:00:00 -0800
tags: [lfnw2021,lfnw2022,COVID-19]
# fb_post_id:
# tweet_id: ?
redirect_to: https://discuss.lfnw.org/t/lfnw2021-cancelled/195/2
---
Dear friends of LinuxFest Northwest,

In the interest of public health and maintaining the traditions of LinuxFest Northwest, we have decided to forgo the event for 2021.

LFNW is both an educational and a social event, and while we would like to find a way to bring the essence of LinuxFest into this socially-distant period, we’re finding that we cannot do so effectively. We will be moving forward with some other exciting projects to make the LFNW experience more cohesive, accessible, and enjoyable for everyone - look for more announcements in the near future.


Rest assured that we are doing everything that we can to bring a fantastic event back next year. We hope that this crisis will have passed by 2022 and look forward to seeing you then.

In Community,<br>
The LFNW organizers.
