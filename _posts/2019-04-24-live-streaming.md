---
layout: post
title:  "Live streaming at LFNW2019!"
date:   2019-04-25 15:00:00 -0700
tags: [LFNW2019, video, live]
permalink: /stream-schedule
redirect_to: https://discuss.lfnw.org/t/live-streaming-at-lfnw2019/147/1
---

Our video team has gone above and beyond this year: in addition to switching to a fully open-source video processing software stack, we're going live streaming some sessions this year!

### Saturday, April 26

Live stream: [https://live.lfnw.org/saturday](https://live.lfnw.org/saturday)

* 9:30am
[**Let's build our own Internet-of-Things** _by Johannes Ernst_](https://lfnw.org/conferences/2019/program/proposals/266)
* 10:45am
[**2019: Fifty years of Unix, Internet and more** _by John 'maddog' Hall_](https://lfnw.org/conferences/2019/program/proposals/247)
* 1:00pm
[**From Sysadmin to DevOps Engineer -- The Easy Way!** _by Ross Brunson_](https://lfnw.org/conferences/2019/program/proposals/226)
* 2:15pm
[**A QUIC History of HTTP Through The Future** _by Bri Hatch_](https://lfnw.org/conferences/2019/program/proposals/213)
* 3:30pm
[**Ubuntu 19.04+: The Disco Dingo and Beyond** _by Simon Quigley_](https://lfnw.org/conferences/2019/program/proposals/208)

### Sunday, April 27

Live stream: [https://live.lfnw.org/sunday](https://live.lfnw.org/sunday)

* 9:30am
[**Command Line for Kids of All Ages** _by Adam Monsen_](https://lfnw.org/conferences/2019/program/proposals/194)
* 10:45am
[**Sex, Secret and God: A Brief History of Bad Passwords** _by Kyle Rankin_](https://lfnw.org/conferences/2019/program/proposals/341)
* 12:30pm
[**From Analog to Digital and Back** _by George Dyson_](https://lfnw.org/conferences/2019/program/proposals/289)
* 1:45pm
[**Linux Gaming - The Dark Ages, Today, And Beyond** _by Ray Shimko_](https://lfnw.org/conferences/2019/program/proposals/215)
* 3:00pm
[**Q&A: Past, Present & Future** _with Jon 'maddog' Hall, Kyle Rankin, Simon Quigley_](https://lfnw.org/conferences/2019/program/proposals/344)
