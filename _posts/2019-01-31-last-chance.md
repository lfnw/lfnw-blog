---
layout: post
title:  "Last Chance to submit proposals for LFNW2019!"
date:   2019-01-31 00:00:00 -0800
tags: [LFNW2019, call-for-paper]
fb_post_id: 10156033332627286
tweet_id: 1091021301819486209
redirect_to: https://discuss.lfnw.org/c/lfnw-blog/10
---

Today is the last day your LFNW talk/tutorial/presentation submitted! Get on over to [https://lfnw.org/conferences/2019/program/proposals/new](https://lfnw.org/conferences/2019/program/proposals/new) and check it off your to-do list. We wouldn't have a fest if it weren't for brilliant folks like you bringing awesome content to share.
